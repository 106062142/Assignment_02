# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description
這是一款雷電遊戲，製作者：106062142黃晨瑋

# Basic Components Description : 
1. Jucify mechanisms :
    (1). 遊戲進程：有開始頁面、遊戲頁面、暫停視窗、死掉畫面、過關畫面(共5關)。每個選單各有一到數種操作
    (2). 玩家可左右移動，每過一關即會進化，另有3種子彈會隨著破關而改變。共三條命，消耗光後會死亡並進入死掉畫面
    (3). 遊戲在每關初始時會隨機生成數隻怪物並隨意排列，並隨著時間進行增加新怪物。當要進到下一關時，現有怪物會被悉數移除，並更新怪物外觀&怪物子彈
    (4). 玩家與怪物皆是中彈就扣一滴血，玩家有3滴血、怪物各有1滴血
    (5). 玩家有一次使用大絕招的機會，會將畫面中所有敵人殺死，以免去死亡危機，但不計分
2. Animations : 
    (1). 使用大絕時，會有另一種特效，且螢幕會震動
    (2). 玩家的子彈&外觀會隨著關卡而改變，怪物有數種外觀型態會不斷交替閃爍
3. Particle Systems : 打到與被打到時，會有用praticle system產生的爆破特效
4. Sound effects : 有背景音樂&放大招的音樂，並且可以調整音量&靜音
5. Appearance:
    (1). 當破此關後，地圖會捲動，帶領玩家到新的一關，每關皆有各自的背景
    (2). 共有5關，每次破關後皆會有所更新：敵人產生速度、敵人移動速度、敵人子彈速度、敵人最大數量、玩家移動數度、玩家發射間隔、最大子彈數量
    (3). UI: 血條、大絕、防護罩、分數欄、顯示等級、分數、音量可調整、可暫停
    (4). 玩家與敵人共有10種外觀、6種子彈，2種爆炸特效，放大絕時螢幕會震動，暫停、開始、輸、贏4種選單

# Bonus Functions Description : 
1. 防護罩 : 每隔一段時間，自動掉落可撿拾，只要接到就可以獲得絕對防禦，在此段時間內不會受到傷害
