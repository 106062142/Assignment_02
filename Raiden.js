var game = new Phaser.Game(800, 600, Phaser.AUTO, 'phaser-example01', { preload: preload, create: create, update: update, render: render });

function preload() {    
    game.load.image('label_welcome', 'assets/label_welcome.png');
    game.load.image('label_win', 'assets/label_win.png');
    game.load.image('label_lose', 'assets/label_lose.png');
    game.load.image('label_pause', 'assets/label_pause.png');
    game.load.image('menu_welcome', 'assets/menu_welcome.png');
    game.load.image('menu_game_end', 'assets/menu_game_end.png');
    game.load.image('menu_pause', 'assets/menu_pause.png');

    game.load.image('start_page', 'assets/start_page.png');
    game.load.image('bgall', 'assets/background_all.png');
    game.load.image('thunder', 'assets/thunder.png');
    
    game.load.image('bubble', 'assets/bubble.png');
    game.load.image('lazer', 'assets/lazer.png');
    game.load.image('plnbullet01', 'assets/plane_bullet01.png');
    game.load.image('plnbullet02', 'assets/plane_bullet02.png');
    game.load.image('plnbullet03', 'assets/plane_bullet03.png');
    game.load.image('plnbullet04', 'assets/plane_bullet04.png');
    game.load.image('enebullet01', 'assets/enemy_bullet01.png');
    game.load.image('enebullet02', 'assets/enemy_bullet02.png');
    game.load.image('enebullet03', 'assets/enemy_bullet03.png');
    
    game.load.spritesheet('explode', 'assets/explode.png', 35.44, 36);
    game.load.spritesheet('plane_all', 'assets/plane_all.png', 116.4, 94); // (582, 94)
    game.load.spritesheet('ene01', 'assets/ene01.png', 85, 78);
    game.load.spritesheet('ene02', 'assets/ene02.png', 85, 78);
    game.load.spritesheet('ene03', 'assets/ene03.png', 85, 78);
    game.load.spritesheet('ene04', 'assets/ene04.png', 140.3, 125); // boss01
    game.load.spritesheet('ene05', 'assets/ene05.png', 130.3, 127); // boss02

    game.load.audio('bgmusic', 'assets/bg_music.ogg');
    game.load.audio('ultimatemusic', 'assets/ultimate_music.mp3');
}

var stateText;
var scoreString = '', scoreText;
var levelString = 'Level : ', levelText;
var label_welcome, label_win, label_lose, label_pause;
var menu_welcome, menu_game_end, menu_pause;

var player, aliens, bullets, bubble, explodeUltimate;
var SCORE = 0, ISPROTECTED = false, USEULTIMATESKILL = false;
var PAUSE = false, GAMEPAGE = ['start_page', 'game_page', 'win_page', 'lose_page'], GAMEPAGEIDX = 0; // start_page & game_page
var cursors;
var lives;
var firingTimer = 0;
var fireButton, attackButton, startButton, quitButton, pauseButton, muteButton, upVolume, downVolume;
var music_1, music_2;

var backgroundfield;
var bg_state_scrollRate = 5, bg_state = [600, 1250, 1880, 2500, 3140], bg_state_idx = 0;
var pln_bull_idx = 1, max_pln_bull = 5,  pln_fire_interval = 600, pln_bull_speed = -400;
var ene_bull_idx = 1, max_ene_bull = 10, ene_fire_interval = 1100, ene_bull_speed = 250;
var plane_idx = 0, pln_move_speed = 210; // plane is a spritesheet so it's index is different to others
var enemy_idx = 1, ene_move_speed = 700, ene_add_interval = 140;
function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);

    // The scrolling backgroundfield background
    backgroundfield = game.add.tileSprite(0, 0, 800, 600, 'bgall');
    backgroundfield.tilePosition.y = bg_state[bg_state_idx];

    // Player's bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    // The enemy's bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    createBullets();
    //  The baddies!
    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;
    createAliens();
    //  The hero!
    player = game.add.sprite(400, 500, 'plane_all');
    player.enableBody = true;
    
    player.anchor.setTo(0.5, 0.5);
    player.animations.add('flying0', [0], 2, true); // 3rd arg: frame rate
    player.animations.add('flying1', [1], 2, true);
    player.animations.add('flying2', [2], 2, true);
    player.animations.add('flying3', [3], 2, true);
    player.animations.add('flying4', [4], 2, true);
    player.play(`flying${plane_idx}`);
    game.physics.enable(player, Phaser.Physics.ARCADE);
    // Text
    stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;
    // Score
    scoreString = 'Score : ';
    scoreText = game.add.text(10, 10, scoreString + SCORE, { font: '34px Arial', fill: '#fff' });
    // Level
    levelText = game.add.text(game.world.width / 2 - 40, 10, levelString + '1', { font: '34px Arial', fill: '#fff' });
    // Lives
    lives = game.add.group();
    game.add.text(game.world.width - 150, 10, 'Lives : ', { font: '34px Arial', fill: '#fff' });

    for (var i = 0; i < 3; i++){
        var ship = lives.create(game.world.width - 120 + (40 * i), 80, 'thunder'); // x, y, image
        ship.anchor.setTo(0.5, 0.5);
        ship.angle = 90;
        ship.alpha = 0.7;
    }

    explodeUltimate = game.add.group();
    explodeUltimate.createMultiple(50, 'explode'); // 2nd arg: image which preload on the top
    explodeUltimate.forEach((e) => {
        e.anchor.x = 0.5;
        e.anchor.y = 0.5;
        e.animations.add('explodeU'); // animation's namte
    }, this);

    this.explosion = game.add.sprite(-100, -100, 'explode');
    this.explosion.animations.add('toExplode', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16], 500, true);
    game.physics.arcade.enable(this.explosion);
    this.explosion.body.immovable = true;

    // Practicle
    this.emitter = game.add.emitter(422, 320, 15);
    this.emitter.makeParticles('explode');
    this.emitter.setYSpeed(-150, 150);
    this.emitter.setXSpeed(-150, 150);
    this.emitter.setScale(2, 0, 2, 0, 800);
    this.emitter.gravity = 500;

    //  And some controls to play the game with
    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    attackButton = game.input.keyboard.addKey(Phaser.KeyCode.Z); // KeyCode not board !!!
    startButton = game.input.keyboard.addKey(Phaser.KeyCode.S);
    quitButton = game.input.keyboard.addKey(Phaser.KeyCode.Q);
    pauseButton = game.input.keyboard.addKey(Phaser.KeyCode.P);
    muteButton = game.input.keyboard.addKey(Phaser.KeyCode.M);
    upVolume = game.input.keyboard.addKey(Phaser.KeyCode.N);
    downVolume = game.input.keyboard.addKey(Phaser.KeyCode.B);

    music_1 = game.add.audio('bgmusic');
    music_2 = game.add.audio('ultimatemusic');
    music_1.play(); // play in game_page
    // music_2.play(); // play when use ultimate attack
    music_1.volume = 50;
    music_2.volume = 150;
}
function changeVolume() {
    if (upVolume.isDown && music_1.volume < 200 ){
        music_1.volume += 1;
    } else if (downVolume.isDown && music_1.volume > 0 ){
        music_1.volume -= 1;
    } else if (muteButton.isDown) {
        if(music_1.mute == false) music_1.mute = true;
        else if(music_1.mute == true) music_1.mute = false;
    }
}
function createBullets() {
    bullets.createMultiple(max_pln_bull, `plnbullet0${pln_bull_idx}`);
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);

    enemyBullets.createMultiple(max_ene_bull, `enebullet0${ene_bull_idx}`);
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);
}
function createAliens () {
    let eneqtt_y = Math.floor(Math.random() * 3 + 1);
    let eneqtt_x, last_pos_x;
    
    for (var y = 0; y < eneqtt_y; y++){
        last_pos_x = 0;
        eneqtt_x = Math.floor(Math.random() * 4 + 1);
        for (var x = 0; x < eneqtt_x; x++){
            last_pos_x += Math.floor(Math.random() * 40 + 90);

            var alien = aliens.create(last_pos_x, y * 50, `ene0${enemy_idx}`);
            alien.anchor.setTo(0.5, 0.5);
            if (enemy_idx <= 3) alien.animations.add('fly', [ 0, 1, 2, 3 ], 2, true); // 3rd arg: speed
            else alien.animations.add('fly', [ 0, 1, 2 ], 2, true); // 3rd arg: speed
            alien.play('fly');
            alien.body.moves = false;
        }
    }
    aliens.x = 70;
    aliens.y = 30;

    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    var do_how_many_times = 1000000, start_moving_timing = 0;
    var tween = game.add.tween(aliens).to({ x: 200 }, ene_move_speed, Phaser.Easing.Linear.None, true, start_moving_timing, do_how_many_times, true);
    // var tween = game.add.tween(aliens).to({ x: 200 }, 0, Phaser.Easing.Linear.None, true, 0, 1000, true);
    //  When the tween loops it calls descend
    tween.onLoop.add(descend, this); // aliens can move without line
}
function createAliensVer2 () {
    let pos_x = Math.floor(Math.random() * 500 + 100);
    let pos_y = Math.floor(Math.random() * 170 + 20);
    
    var alien = aliens.create(pos_x, pos_y, `ene0${enemy_idx}`);
    alien.anchor.setTo(0.5, 0.5);
    alien.animations.add('fly', [0, 1, 2, 3 ], 2, true); // 3rd arg: speed
    alien.play('fly');
    alien.body.moves = false;
    // can't add tween here!!!
}
function createBubble(){
    let pos_x = Math.floor(Math.random() * 650 + 50);
    let pos_y = Math.floor(Math.random() * 120 + 20);
    bubble = game.add.sprite(pos_x, pos_y, 'bubble');
    bubble.anchor.setTo(0.5, 0.5);
    game.physics.enable(bubble, Phaser.Physics.ARCADE);
    bubble.body.velocity.y = 400;
}
function descend() {
    aliens.y += 10;
}

var cnttime = 0;
var pause_interval = 350, prev_cnt = 0;
var hasDisplayed01 = false, hasDisplayed02 = false, hasDisplayed03 = false;
function update() {
    changeVolume();
    if (GAMEPAGE[GAMEPAGEIDX] == 'start_page'){
        if (hasDisplayed01 == false){
            music_1.stop();
            start_page = game.add.image(0, 0, 'start_page');
            label_welcome = game.add.image(220, 150, 'label_welcome');
            menu_welcome = game.add.image(235, 315, 'menu_welcome');
            hasDisplayed01 = true;
        }
        if( startButton.isDown ){
            music_1.play();
            hasDisplayed01 = false;
            GAMEPAGEIDX = 1; // go to game_page
            start_page.destroy();
            label_welcome.destroy();
            menu_welcome.destroy();
        }
    } else if (GAMEPAGE[GAMEPAGEIDX] == 'win_page') {
        if (hasDisplayed02 == false){
            music_1.stop();
            player.body.velocity.x = 0; // !!!!!!!
            enemyBullets.callAll('kill',this);
            label_win = game.add.image(220, 100, 'label_win');
            menu_win = game.add.image(225, 265, 'menu_game_end');
            //the "click to restart" handler
            // game.input.onTap.addOnce(restart,this); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            hasDisplayed02 = true;
        }
        if (startButton.isDown) {
            music_1.play();
            GAMEPAGEIDX = 1; // go to game_page
            hasDisplayed02 = false;
            label_win.destroy();
            menu_win.destroy();
            restart();
        } else if (quitButton.isDown) {
            music_1.play(); // and it will be stop in start_page
            GAMEPAGEIDX = 0; // go to start_page
            hasDisplayed02 = false;
            label_win.destroy();
            menu_win.destroy();
            restart();
        }
    } else if (GAMEPAGE[GAMEPAGEIDX] == 'lose_page'){
        if (hasDisplayed03 == false) {
            music_1.stop();
            player.kill();
            enemyBullets.callAll('kill');
            label_lose = game.add.image(220, 100, 'label_lose');
            menu_lose = game.add.image(225, 265, 'menu_game_end');
            hasDisplayed03 = true;
        }
        if (startButton.isDown) {
            music_1.play();
            GAMEPAGEIDX = 1; // go to game_page
            hasDisplayed03 = false;
            label_lose.destroy();
            menu_lose.destroy();
            restart();
        } else if(quitButton.isDown){
            music_1.play(); // and it will be stop in start_page
            GAMEPAGEIDX = 0; // go to start_page
            hasDisplayed03 = false;
            label_lose.destroy();
            menu_lose.destroy();
            restart();
        }
    } else if (GAMEPAGE[GAMEPAGEIDX] == 'game_page'){
        if (pauseButton.isDown && PAUSE == false && game.time.now - prev_cnt > pause_interval){
            PAUSE = true;
            prev_cnt = game.time.now;
            label_pause = game.add.image(220, 100, 'label_pause');
            menu_pause = game.add.image(225, 265, 'menu_pause');
        }
        if(PAUSE == true){
            enemyBullets.removeAll();
            bullets.removeAll();
            if (pauseButton.isDown && PAUSE == true && game.time.now - prev_cnt > pause_interval) {
                PAUSE = false;
                prev_cnt = game.time.now;
                label_pause.destroy();
                menu_pause.destroy();
                createBullets();
            } else if (quitButton.isDown ){
                prev_cnt = game.time.now;
                label_pause.destroy();
                menu_pause.destroy();
                restart();
            }
            return;
        }

        if (SCORE == 100) levelUp(0);
        else if (SCORE == 200) levelUp(1);
        else if (SCORE == 300) levelUp(2);
        else if (SCORE == 400) levelUp(3);
        
        if (player.alive){
            // console.log(cnttime);
            if( cnttime == 4000000 ) cnttime = 0;
            else cnttime++;
            if (cnttime % ene_add_interval == 0) createAliensVer2(); // Add new enemies
            if (ISPROTECTED == false && cnttime % 300 == 0) createBubble();
            else if (ISPROTECTED == true && cnttime % 600 == 0) removeBubble();

            player.body.velocity.setTo(0, 0); //  Reset the player, then check for movement keys
            if (cursors.left.isDown) player.body.velocity.x = -pln_move_speed;
            else if (cursors.right.isDown) player.body.velocity.x = pln_move_speed;

            // Firing
            if (fireButton.isDown) fireBullet();
            if (game.time.now > firingTimer) enemyFires();
            if(USEULTIMATESKILL == false && attackButton.isDown == true) fireUltimate();

            // Run collision
            game.physics.arcade.overlap(player, enemyBullets, enemyHitsPlayer, null, this); // collide or overlap is okay
            game.physics.arcade.collide(bullets, aliens, collisionHandler, null, this);
            game.physics.arcade.collide(player, bubble, getBubble, null, this);
        }
    }
} // end of update

function render() {
    // for (var i = 0; i < aliens.length; i++){
    //     game.debug.body(aliens.children[i]);
    // }
}
function levelUp (currLevel) {
    if (bg_state_idx == currLevel) {
        aliens.removeAll();
        enemyBullets.removeAll();
        bullets.removeAll();
    }
    bg_state_idx = currLevel + 1;
    
    if (backgroundfield.tilePosition.y < bg_state[bg_state_idx]) backgroundfield.tilePosition.y += bg_state_scrollRate; // Scroll the background
    if (backgroundfield.tilePosition.y == bg_state[bg_state_idx] - bg_state_scrollRate) {
        levelText.destroy();
        var tmp = currLevel + 2
        levelText = game.add.text(game.world.width / 2 - 40, 10, levelString + tmp, { font: '34px Arial', fill: '#fff' });

        plane_idx = currLevel + 1;
        enemy_idx++;
        player.play(`flying${plane_idx}`);
        createAliens();
        if(currLevel == 0) pln_bull_idx = 1;
        else if(currLevel < 2) pln_bull_idx = 2;
        else pln_bull_idx = 3;
        if (currLevel < 2) ene_bull_idx = 2; // level 1 is ene_bull_idx = 1, but this case won't appear in this function
        else ene_bull_idx = 3;
        createBullets();

        pln_fire_interval -= 115;
        ene_fire_interval -= 240;
        max_pln_bull += 2;
        max_ene_bull += 1;
        pln_move_speed += 50;
        ene_move_speed -= 100; // 700, 600, 500, 400, 300
        ene_add_interval -= 25;
    }
}
function getBubble(player, bubble) { //  When a bullet hits an alien we kill them both
    ISPROTECTED = true;
    bubble.reset(player.body.x + 50, player.body.y + 50); // And fire the bullet from this enemy
}
function removeBubble(){
    if(ISPROTECTED == true) ISPROTECTED = false;
    bubble.kill();
}
function collisionHandler (bullet, alien) { //  When a bullet hits an alien we kill them both
    bullet.kill();
    alien.kill();

    // Particle System
    this.emitter.x = alien.body.x + 35;
    this.emitter.y = alien.body.y + 30;
    this.emitter.start(true, 800, null, 15);

    SCORE += 20;
    scoreText.text = scoreString + SCORE;

    if( SCORE > 590 ) GAMEPAGEIDX = 2; // go to win_page
    // if (aliens.countLiving() == 0){
    //     SCORE += 1000;
    //     scoreText.text = scoreString + SCORE;
    //     stateText.text = " You Won, \n Click to restart";
    //     stateText.visible = true;
    //     enemyBullets.callAll('kill',this);
    //     game.input.onTap.addOnce(restart,this); //the "click to restart" handler !!!!!!!!!!!!!!!!!!!!!!!!
    // }
}

function enemyHitsPlayer (player, bullet) {
    bullet.kill();
    if( ISPROTECTED == true ) return;
    
    live = lives.getFirstAlive();
    if (live) live.kill(); // reduce 1 life

    // Particle System
    this.emitter.x = player.body.x + 45;
    this.emitter.y = player.body.y + 40; 
    this.emitter.start(true, 800, null, 15);

    // When the player dies
    if (lives.countLiving() < 1){
        GAMEPAGEIDX = 3; // go to lose_page
        // stateText.text=" GAME OVER \n Click to restart";
        // stateText.visible = true;
        // game.input.onTap.addOnce(restart,this); // the "click to restart" handler!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    }
}

var livingEnemies = [];
function enemyFires () {
    var enemyBullet = enemyBullets.getFirstExists(false); //  Grab the first bullet we can from the pool
    livingEnemies.length = 0; // This is used to clear the array, so this variable can be announced inside function

    aliens.forEachAlive(function(alien){
        livingEnemies.push(alien); // put every living enemy in an array
    });

    if (enemyBullet && livingEnemies.length > 0){
        var random = game.rnd.integerInRange(0,livingEnemies.length - 1);
        var shooter = livingEnemies[random]; // randomly select one of them
        
        enemyBullet.reset(shooter.body.x, shooter.body.y); // And fire the bullet from this enemy

        game.physics.arcade.moveToObject(enemyBullet, player, ene_bull_speed);
        firingTimer = game.time.now + ene_fire_interval;
    }
}
var bulletTime = 0;
function fireBullet () {
    // To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime){
        // Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);

        if (bullet){ //  And fire it
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = pln_bull_speed;
            bulletTime = game.time.now + pln_fire_interval;
        }
    }
}
function fireUltimate(){
    USEULTIMATESKILL = true;
    aliens.forEachAlive(function (alien) {
        if(alien != undefined){
            game.camera.shake(0.03, 1200);
            music_2.play();
            var expd = explodeUltimate.getFirstExists(false);
            expd.reset(alien.body.x + 10, alien.body.y + 35);
            expd.play('explodeU', 30, false, true);
            setTimeout(() => {
                expd.kill(); // They must be reused !!!!!
                alien.kill();
            }, 800);
            setTimeout(() => {
                music_2.stop();
            }, 1000);

            // game.emitter.x = alien.body.x + 35; // TODO:
            // this.emitter.y = alien.body.y + 30;
            // game.emitter.start(true, 800, null, 15);
        }
    });
}
function restart () { //  A new level starts
    SCORE = 0;
    scoreText.destroy();
    scoreText = game.add.text(10, 10, scoreString + SCORE, { font: '34px Arial', fill: '#fff' });
    levelText.destroy();
    levelText = game.add.text(game.world.width / 2 - 40, 10, levelString + '1', { font: '34px Arial', fill: '#fff' });
    
    PAUSE = false, GAMEPAGEIDX = 0;
    bg_state_idx = 0;
    pln_bull_idx = 1, pln_fire_interval = 600, pln_bull_speed = -400;
    ene_bull_idx = 1, ene_fire_interval = 1200, ene_bull_speed = 250;
    enemyBullets.removeAll();
    bullets.removeAll();
    createBullets();

    if (bubble != undefined) bubble.kill();
    ISPROTECTED = false;
    USEULTIMATESKILL = false;

    plane_idx = 0, pln_move_speed = 200;   
    enemy_idx = 1, ene_move_speed = 700, ene_add_interval = 140;
    
    lives.callAll('revive'); //resets the life count    
    aliens.removeAll();
    createAliens(); //  And brings the aliens back from the dead :)

    player.revive(); //revives the player
    player.play(`flying${plane_idx}`);
    stateText.visible = false; //hides the text

    backgroundfield.tilePosition.y = bg_state[bg_state_idx];
    music_1.volume = 50;
    music_2.volume = 150;
}
