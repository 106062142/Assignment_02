from PIL import Image # import Image is illegal
import os, fnmatch

def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern): result.append( os.path.join(root, name) )
    return result

Root = r'C:\Users\cwHuang\Desktop\Assignment_02_106062142\assets'
res = find('ene0*', Root)  # TODO:
# print(res, "!!!!!!!!!!!!!!!!!")

cnt = 0
for p in res:
    img = Image.open(p)
    print(img.format, img.mode, img.size, '\n')

    # width = 840  # TODO:
    # height = int( ( float(width) / img.size[0] ) * img.size[1] )

    # p = p[0:-5]
    # r = os.path.join('', p + str(cnt + 1) + '.png') # root, file_name

    # img = img.resize((width, height), Image.BILINEAR)
    # img.save(r, "png")

    cnt += 1
'''
1. img.save("PIL_test", "png") OR img.save("PIL_test.png")
2. quality: 0 ~ 100, default: 75
3. resize(): 1st arg is tuple, 2nd arg default value is Image.NEARIST, but BILINEAR has better quality
'''